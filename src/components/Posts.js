import React, {Component} from 'react';
import Listed from "./Listed";

class Posts extends Component{
    render(){
        return(
            <div className={"col-12 col-md-8"}>
                <h2 className={"text-center"}>Posts</h2>
                <Listed
                    posts={this.props.posts}
                    deletePosts = {this.props.deletePosts}
                />
            </div>
        )
    }
}

export default Posts;
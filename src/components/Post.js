import React from 'react';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2';

class Post extends React.Component{

    confirmDeletePost = () => {

        const {id} = this.props.info;

        Swal.fire({
            title: 'Estas seguro?',
            text: "No se podra deshacer el cambio",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI, Borrar'
        }).then((result) => {
            if (result.value) {
                this.props.deletePosts(id);

                Swal.fire(
                    'Borrado!',
                    'Tu post ha sido borrado.',
                    'success'
                )
            }
        })
    };


    render(){

        const {id, title} = this.props.info;

        return(
            <tr>
                <td>{id}</td>
                <td>{title}</td>
                <td>
                    <Link to={`/post/${id}`} className={"btn btn-primary"}>Show</Link>
                    <Link to={`/edit/${id}`} className={"btn btn-warning"}>Edit</Link>
                    <button onClick={this.confirmDeletePost} type={"button"} className={"btn btn-danger"}>Delete</button>
                </td>
            </tr>
        )
    }
}

export default Post;
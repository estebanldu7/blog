import React from 'react';
import {Link} from 'react-router-dom'
import '../css/navigation.css';

const Navigation = () => {
    return(
        <nav className={"col-12 col-md-8"}>
            <Link to={"/"}>Todos Los Post</Link>
            <Link to={"/create"}>Nuevo Post</Link>
        </nav>
    )
}

export default Navigation;


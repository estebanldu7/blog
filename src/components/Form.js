import React, {Component} from 'react';

class Form extends Component {

    titleRef = React.createRef();
    contentRef = React.createRef();

    createPost = (e) => {

        e.preventDefault();

        const post ={
            title: this.titleRef.current.value,
            content: this.contentRef.current.value,
            userId: 1
        };

        this.props.createPost(post);
    };

    render(){
        return(
          <form onSubmit={this.createPost} className={"col-8"}>
              <legend className={"text-center"}> Crear nuevo post </legend>
              <div className={"form-group"}>
                  <label>Titulo del post</label>
                  <input ref={this.titleRef} type={"text"} className={"form-control"} placeholder={"Titulo del post"} />

                  <label>Contenido</label>
                  <textarea ref={this.contentRef} className={"form-control"}></textarea>
              </div>

              <button type={"submit"} className={"btn btn-primary"}>Crear</button>
          </form>
        )
    }
}

export default Form;
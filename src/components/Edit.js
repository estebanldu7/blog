import React, {Component} from 'react';

class Edit extends Component {

    titleRef = React.createRef();
    contentRef = React.createRef();

    editPost = (e) => {

        e.preventDefault();

        const post ={
            title: this.titleRef.current.value,
            body: this.contentRef.current.value,
            id: this.props.post.id,
            userId: 1
        };

        this.props.editPost(post);
    };


    fillForm = () => {

        if(!this.props.post){
            return null;
        }

        const {title, body} =  this.props.post;

        return(
            <form onSubmit={this.editPost} className={"col-8"}>
                <legend className={"text-center"}> Editar post </legend>
                <div className={"form-group"}>
                    <label>Titulo del post</label>
                    <input ref={this.titleRef} type={"text"} className={"form-control"} defaultValue={title} />

                    <label>Contenido</label>
                    <textarea ref={this.contentRef} className={"form-control"} defaultValue={body}></textarea>
                </div>

                <button type={"submit"} className={"btn btn-primary"}>Editar</button>
            </form>
        )
    };


    render(){
        return(
            <React.Fragment>
                {this.fillForm()}
            </React.Fragment>
        )
    }
}

export default Edit;
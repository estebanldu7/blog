import React, {Component} from 'react';
import {BrowserRouter, Route, Switch } from 'react-router-dom';
import Swal from 'sweetalert2';
import Navigation from './Navigation';
import SinglePost from "./SinglePost";
import Header from './Header';
import Posts from './Posts';
import Form from "./Form";
import Edit from "./Edit";
import axios from 'axios';

class Router extends Component{

    state = {
        posts : []
    };

    componentDidMount(){
        this.getPosts();
    }

    getPosts = () => {
        axios.get(`https://jsonplaceholder.typicode.com/posts`).then(resp =>{

            this.setState(() => {
                return {posts : resp.data};
            });
        }).catch(
            console.log("Ha ocurrido un error ")
        )
    };

    deletePosts = (id) => {
        axios.delete(`https://jsonplaceholder.typicode.com/posts/{id}`).then(resp => {
            if(resp.status === 200){

                const posts = [...this.state.posts];
                let result = posts.filter(post => (
                    post.id !== id
                ));

                this.setState(() => {
                    return {posts : result};
                });
            }
        })
    };

    createPost = (post) => {
        axios.post(`https://jsonplaceholder.typicode.com/posts`, {post}).then(resp => {
            if(resp.status === 201) {

                Swal.fire(
                    'Post Creado!',
                    'El post se creo correctamente.!',
                    'success'
                );

                let postId = {id: resp.data.id};
                const newPost = Object.assign({}, resp.data.post, postId);

                this.setState((prevState) => {
                    return {posts : [...prevState.posts, newPost]};
                });
            }
        })
    };

    editPost = (editedPost) => {
        const {id} = editedPost;

        axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {editedPost}).then(resp => {
            if(resp.status === 200){

                Swal.fire(
                    'Post Actualizado!',
                    'Se guardo correctamente.',
                    'success'
                );

                let postId = resp.data.id;

                const posts = [...this.state.posts];
                const editPost =  posts.findIndex(post => postId === post.id);
                posts[editPost] = editedPost;

                this.setState(() => {
                    return {posts : posts};
                });
            }
        })


    };

    //Component is for static route and render is for dynamic page.
    render(){
        return(
            <BrowserRouter>
               <div className={"container"}>
                   <div className={"row justify-content-center"}>
                      <Header/>
                      <Navigation/>
                      <Switch>
                            <Route exact path={"/"} render={() => {
                                return(
                                    <Posts
                                        posts={this.state.posts}
                                        deletePosts = {this.deletePosts}
                                    />
                                )
                            }}/>

                           <Route exact path={"/post/:postId"} render={(props) => {
                               let idPost = props.location.pathname.replace('/post/', '');
                               const posts = this.state.posts;
                               let filter;

                               filter = posts.filter(post => (
                                    post.id === Number(idPost)
                               ));

                               return(
                                   <SinglePost
                                        posts={filter[0]}
                                   />
                               )
                           }}/>

                           <Route exact path={"/create"} render={() => {
                               return(
                                   <Form
                                       createPost={this.createPost}
                                   />
                               )
                           }}/>

                           <Route exact path={"/edit/:postId"} render={(props) => {
                               let idPost = props.location.pathname.replace('/edit/', '');
                               const posts = this.state.posts;
                               let filter;

                               filter = posts.filter(post => (
                                   post.id === Number(idPost)
                               ));

                               return(
                                   <Edit
                                       post={filter[0]}
                                       editPost={this.editPost}
                                   />
                               )
                           }}/>
                       </Switch>
                   </div>
               </div>
            </BrowserRouter>
        )
    }
}

export default Router;

